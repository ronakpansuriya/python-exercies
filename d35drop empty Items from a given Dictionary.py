color = {'c1': 'Red', 'c2': 'Green', 'c3': None}

# color['c3']= ['blue']
print(color)
result = {key:value for key,value in color.items() if value is not None}
print(result)