from itertools import groupby

def group_num(l_nums):
    return [[len(list(group)),key]for key,group in groupby(l_nums)]

n_list = [1,1,2,3,4,4.3,5, 1]
print(group_num(n_list))