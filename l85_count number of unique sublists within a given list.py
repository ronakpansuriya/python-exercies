def unique(input_list):
    result = {}
    for i in input_list:
        result.setdefault(tuple(i),list()).append(1)
    for a,b in result.items():
        result[a] = sum(b)
    return result

list1 = [[1, 3], [5, 7], [1, 3], [13, 15, 17], [5, 7], [9, 11]] 
print(unique(list1))
