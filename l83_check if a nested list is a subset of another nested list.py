i = [[1, 3], [5, 7], [9, 11], [13, 15, 17]]
j = [[1, 3], [13, 15, 17]]
def check(i,j):
    return all(map(i.__contains__,j))
print(check(i,j))
list1 = [
           [
             [1,2],[2,3]
           ],
           [
             [3,4],[5,7]
           ]
         ]
list2 = [
           [
             [3,4], [5, 6]
           ]
         ]
print(check(list1,list2))