color = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
def group(l):
    result = {}
    for key,value in l:
        result.setdefault(key,[]).append(value)
    return result
print(group(color))