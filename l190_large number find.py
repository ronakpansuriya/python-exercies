l1 = [1, 2, 3, 4, 5, 6]
l2 = [3, 6, 8, 9, 10, 6]

def large(l1,l2,n):
    result = sorted([x*y for x in l1 for y in l2 ],reverse = True)[:n]
    
    return result

print(large(l1,l2,3))