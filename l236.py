def test(lst,key):
    return [x.get(key) for x in lst]

lst =[ { 'name': 'Areeba', 'age': 8 },
  { 'name': 'Zachariah', 'age': 36 },
  { 'name': 'Caspar', 'age': 34 },
  { 'name': 'Presley', 'age': 10 }
]

print(test(lst,'age'))