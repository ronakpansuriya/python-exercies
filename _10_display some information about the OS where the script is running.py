import platform as pi
os_profile = [
    'architecture',
        'linux_distribution',
        'mac_ver',
        'machine',
        'node',
        'platform',
        'processor',
        'python_build',
        'python_compiler',
        'python_version',
        'release',
        'system',
        'uname',
        'version',
]
for key in os_profile:
    if hasattr(pi,key):
        print(key+":" +str(getattr(pi,key)()))