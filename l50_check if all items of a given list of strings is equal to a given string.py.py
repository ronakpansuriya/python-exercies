
lst = ['ronak',"raj","vivek"]

print(any(c=='ronak' for c in lst))
print(all(c=='eee' for c in lst))

color1 = ["green", "orange", "black", "white"]
color2 = ["green", "green", "green", "green"]

print(all(c == 'blue' for c in color1))
print(all(c == 'green' for c in color2))
