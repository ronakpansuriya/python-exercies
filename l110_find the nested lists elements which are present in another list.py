def nested(l1,l2):
    result = [[n for n in lst if n in l1] for lst in l2]
    return result
num1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
num2 = [[12, 18, 23, 25, 45], [7, 11, 19, 24, 28], [1, 5, 8, 18, 15, 16]]

print(nested(num1,num2))

