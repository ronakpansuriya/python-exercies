import random
sub = ['Python', 'list', 'exercises', 'practice', 'solution']
def latter(word):
    word = list(word)
    random.shuffle(word)
    return ''.join(word)
result = [latter(w) for w in sub]
print(result)
