def check(color, c):
    result = any(c in tu for tu in color)
    return result
color = (
    ('Red', 'White', 'Blue'),
    ('Green', 'Pink', 'Purple'),
    ('Orange', 'Yellow', 'Lime'),
)
print(color)

c1 = 'White'
print(check(color,c1))
