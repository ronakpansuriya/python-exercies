lst = [[1, 2, 3, 5], [2, 3, 5, 4], [0, 5, 4, 1], [3, 7, 2, 1], [1, 2, 1, 2]]

# result = []
def test(lst):
    result = set(l for i in lst for l in i )
    return list(result)

print(test(lst))