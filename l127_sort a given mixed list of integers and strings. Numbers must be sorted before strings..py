lst = [19, 'red', 12, 'green', 'blue', 10, 'white', 'green', 1]

def test(lst):
    int_num = sorted([i for i in lst if type(i) is int])
    str_num = sorted([i for i in lst if type(i) is str])
    return int_num + str_num

print(test(lst))
 