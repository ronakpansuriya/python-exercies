def longest_item(*lst):
    return max(lst,key=len)
print(longest_item('this', 'is', 'a', 'Green'))  
print(longest_item([1, 2, 3], [1, 2], [1, 2, 3, 4, 5])) 
print(longest_item([1, 2, 3, 4], 'Red'))
