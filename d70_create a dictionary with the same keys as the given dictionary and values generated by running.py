student = {'Theodore': {'user': 'Theodore', 'age': 45}, 'Roxanne': {'user': 'Roxanne', 'age': 15}, 'Mathew': {'user': 'Mathew', 'age': 21}}

def test(lst,key):
    return dict((k,key(v))for k,v in lst.items())

print(test(student, lambda x : x['age']))
